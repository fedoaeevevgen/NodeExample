
var log = require('./../libs/log')(module);

module.exports=function(req, res){
    try{
    console.log(req.session.user);
    req.session.destroy(function() {});//Удаление сессии
    res.send("Твоя сессия удалена");}
    catch(err){
        log.error(err);
        res.send('что-то пошло не так');
    }
}